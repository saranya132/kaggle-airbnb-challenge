import pandas as pd
from pandas import DataFrame as df
#import numpy as np
from sklearn.preprocessing import LabelEncoder as LE
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import pickle
import math
from collections import Counter
import csv

test_users = pd.read_csv('test_users.csv')
train_users = pd.read_csv('train_users_2.csv')
#sessions = pd.read_csv('sessions.csv')


##########################training the model###############################
#data = train_users[['date_first_booking','age','signup_method','signup_flow','language','affiliate_channel','affiliate_provider','first_affiliate_tracked','signup_app','first_device_type','first_browser']]
train_users['date_first_booking'] = train_users['date_first_booking'].notnull().astype('int')
train_users['age'] = train_users['age'].fillna(0)
Y = train_users[['country_destination']]
X = train_users[['age','signup_method','signup_flow','language','affiliate_channel','affiliate_provider','first_affiliate_tracked','signup_app','first_device_type', 'first_browser']]
#X["activity"] = pd.Series(session_count)
#X['activity'].fillna(0, inplace = True)
test_users['date_first_booking'] = test_users['date_first_booking'].notnull().astype('int')
test_users['age'] = test_users['age'].fillna(0)
test_x =  test_users[['age','signup_method','signup_flow','language','affiliate_channel','affiliate_provider','first_affiliate_tracked','signup_app','first_device_type','first_browser']]
#test_x["activity"] = pd.Series(session_count)
#test_x['activity'].fillna(0, inplace = True)
test_x.to_csv('processed_in_test_xgboost.csv')

scores_5fold = []
print "For 5-fold cross validation:"
X= X.as_matrix()
Y = Y.as_matrix()
Y = Y.ravel()
test_x = test_x.as_matrix()
clf = RandomForestClassifier(criterion = 'entropy', min_samples_leaf = 40, n_estimators=60)
for i in range(5):
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)
    clf = clf.fit(X_train, Y_train)
    scores_5fold.append(clf.score(X_test, Y_test))

filename = 'finalized_model_randomforest.sav'
s = pickle.dump(clf, open(filename, 'wb'))

##########load//////////////////////////
filename = 'finalized_model_randomforest.sav'
clf = pickle.load(open(filename, 'rb'))
predictions = clf.predict_proba(test_x)
##//////////////////////////////////////////////////////////////////////////

p =[]

for i in predictions:
    p.append(sorted(zip(clf.classes_, i), key=lambda x: x[1], reverse=True)[:5])

pred = [[row[0] for row in prediction] for prediction in p]

print pred

with open('predict_without_randomforest.csv', 'w') as f:
    rows = pred
    writer = csv.writer(f)
    writer.writerows(rows)


infile = open('predict_without_randomforest.csv', 'r')
outfile = open('pwrf.csv', 'w')
c = 0
for line in infile:
    outfile.write(test_users.ix[c,"id"] + "," + line)
    c += 1
infile.close()
outfile.close()

infile = open('pwrf.csv', 'r')
outfile = open('final_pwrf.txt', 'w')
outfile.write("id"+","+"country"+"\n")
for lines in infile:
    line = lines.split(',')
    print line
    for i in range(1,6):
        if i<=4:
            outfile.write(line[0]+ ","+line[i]+ "\n")
        else:
            outfile.write(line[0]+ ","+line[i])
